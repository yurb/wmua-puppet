include '::mysql:server'

define website ($has_mysql_db = true) {
  /* Replace dots with underscores in title */
  $safe_title = regsubst($title, '\.', '_', 'G')
  
  apache::vhost { "${title}":
    port => 80,
    docroot => "/var/www/${title}",

    /* It is better to have the files owned not by root */
    docroot_owner => "apache",
    docroot_group => "apache",
  }

  if $has_mysql_db {
    mysql::db { "${safe_title}":
      user => "${safe_title}",
      password => hiera("${safe_title}.db_password"),
    }
  }
}

class webserver {
  /* puppetlabs-mysql doesn't install these for some reason */
  package { 'mariadb-server':
    ensure => 'present',
  }
  service { 'mariadb':
    ensure => 'running',
    enable => true,
  }
  class { 'apache':
    default_vhost => false,
  }
}
