# Setting up
The contents of this repository is supposed to be a puppet environment. To use it, run the following as root:

    cd /etc/puppet/
    mkdir environments
    cd environments
    git clone https://gitlab.com/yurb/wmua-puppet.git production
    
We also need to configure hiera, which doesn't yet support a per-environment `hiera.yaml` file. Therefore:

    cd /etc/puppet
    ln -s environments/production/hiera.yaml .

Finally, this configuration requires a couple external puppet modules. To install them:

    for module in puppetlabs-{apache,mysql}; do puppet module install $module; done

Then to apply the configuration:

    puppet apply --environment=production /etc/puppet/environments/production/manifests/

This has been tested with Fedora 23 Server.

# Mysql passwords
This configuration uses hiera to store sensitive data like passwords out of version control.

You need to get the `hieradata/common.yaml` separately or create it. An example content of the file:

    ---
    mysite_org_ua:
        db_password: 'some-random-characters'

# Adding virtual hosts
There is a `website` class that creates a virtual host and a database for a website. Adding `website {'mysite.org.ua': }` to the `site.pp` file will create:

- An apache virtual host for `mysite.org.ua` with `/var/www/mysite.org.ua` as the docroot
- `mysite_org_ua` mysql database
- `mysite_org_ua` mysql user with all permission for the above database

You will need to provide the password for the database in hiera as described above.
